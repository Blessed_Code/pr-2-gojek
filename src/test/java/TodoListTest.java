
import org.junit.Test;

import static org.junit.Assert.*;

public class TodoListTest {
    @Test
    public void testDisplayTask() {
        String expectedString = "1. Do dishes -> Makan [DONE]";
        Task task = new Task();
        task.setId(1L);
        task.setName("Do dishes");
        task.setCategory("Makan");
        task.setStats("DONE");

        assertEquals(expectedString, task.toString());
    }

    @Test
    public void testDeleteTask() {
        TodoList todolist = new TodoList();
        todolist.addToDoList(1L, "Do dishes", "Makan");
        todolist.deleteToDoList(1L);

        assertNull(todolist.getTaskById(1L));
    }

    @Test
    public void testSearchTaskByCategory() {
        TodoList todolist = new TodoList();
        String expectedString = "1. Do dishes -> Makan [NOT DONE]";
        todolist.addToDoList(1L, "Do dishes", "Makan");

        assertEquals(expectedString, todolist.getTaskByCategory("Makan"));
    }

    @Test
    public void testSearchTaskById() {
        TodoList todolist = new TodoList();
        String expectedString = "1. Do dishes -> Makan [NOT DONE]";
        todolist.addToDoList(1L, "Do dishes", "Makan");

        assertEquals(expectedString, todolist.getTaskById(1L));
    }

    @Test
    public void testUpdateTask() {
        TodoList todolist = new TodoList();
        String expectedString = "1. Do dishes -> Makan [DONE]";
        todolist.addToDoList(1L, "Do dishes", "Makan");
        todolist.updateToDoList(1L);

        assertEquals(expectedString, todolist.getTaskById(1L));
    }
}