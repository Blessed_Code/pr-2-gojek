
import java.util.ArrayList;

public class TodoList {
    private ArrayList<Task> listOfTask = new ArrayList<>();

    public void addToDoList(Long id, String name, String category) {
        Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setCategory(category);
        task.setStats("NOT DONE");
        listOfTask.add(task);
    }

    public String getToDoList() {
        String theToDoList="";
        for(int i=0; i < listOfTask.size(); i++) {
            theToDoList+= listOfTask.get(i).toString() + "\n";
        }
        return theToDoList;
    }

    public String deleteToDoList(Long id) {
        for(Task task: listOfTask) {
            if(task.getId() == id) {
                listOfTask.remove(task);
                return "successfully deleted";
            }
        }
        return null;
    }

    public String getTaskById(Long id) {
        for(Task task: listOfTask) {
            if(task.getId() == id) {
                return task.toString();
            }
        }
        return null;
    }

    public String getTaskByCategory(String category) {
        for(Task task: listOfTask) {
            if(task.getCategory() == category) {
                return task.toString();
            }
        }
        return null;
    }

    public void updateToDoList(Long id) {
        for(Task task: listOfTask) {
            if(task.getId() == id) {
                task.setStats("DONE");
            }
        }
    }
}
