
import java.util.*;

public class Main {
    private static int menu;
    static TodoList todolist = new TodoList();
    public static void main(String args[]) {
        do{
            System.out.println("1. Add");
            System.out.println("2. Delete");
            System.out.println("3. Search by category");
            System.out.println("4. Search ID");
            System.out.println("5. Update Todo List");
            System.out.println("6. Display Todo List");
            System.out.println("7. Exit");
            System.out.println("Choose >> ");
            Scanner input = new Scanner(System.in);
            menu = input.nextInt();

            if(menu == 1) {
                System.out.println("Input ID");
                Long id = input.nextLong();
                input.nextLine();
                System.out.println("Input task");
                String name = input.nextLine();
                System.out.println("Input category");
                String category = input.nextLine();
                todolist.addToDoList(id, name, category);
            }
            else if(menu == 2) {
                System.out.println("Input ID task yang ingin dihapus");
                Long id = input.nextLong();
                input.nextLine();
                todolist.deleteToDoList(id);
            }
            else if(menu == 3) {
                System.out.println("Input category");
                String category = input.nextLine();
                todolist.getTaskByCategory(category);
            }
            else if(menu == 4) {
                System.out.println("Input ID task yang ingin dicari");
                Long id = input.nextLong();
                input.nextLine();
                todolist.getTaskById(id);
            }
            else if(menu == 5) {
                System.out.println("Input ID task yang ingin di update");
                Long id = input.nextLong();
                input.nextLine();
                todolist.updateToDoList(id);
            }
            else if(menu == 6) {
                System.out.println(todolist.getToDoList());
            }
        }while(menu != 7);
    }
}



